var webApp = exports;

webApp.App = require("./App");
webApp.Plugin = require("./Plugin");

webApp.Locales = require("./plugins/Locales");
webApp.ReduxDevTools = require("./plugins/ReduxDevTools");
webApp.Router = require("./plugins/Router");
webApp.Screen = require("./plugins/Screen");
