var immutable = require("immutable"),
    request = require("@nathanfaucett/request"),
    cookies = require("@nathanfaucett/cookies"),
    page = require("@nathanfaucett/page"),
    once = require("@nathanfaucett/once"),
    eventListener = require("@nathanfaucett/event_listener"),
    State = require("@nathanfaucett/state-immutable"),
    storage = require("@nathanfaucett/storage"),
    createForm = require("@nathanfaucett/create_form"),
    Locales,
    ReduxDevTools,
    Router,
    Screen;

var fromJS = immutable.fromJS,
    AppPrototype;

function App(config, options) {
    var _this = this;

    options = options || {};
    options.Locales = options.Locales || {};
    options.ReduxDevTools = options.ReduxDevTools || {};
    options.Router = options.Router || {};
    options.Screen = options.Screen || {};

    State.call(this);

    this._initted = false;
    this._plugins = {};

    this.appStateKey = options.appStateKey || "WebAppState";
    this.config = config;
    this.options = options;

    this.cookies = cookies;
    this.page = page;
    this.request = request;
    this.storage = storage;

    if (!options.Locales.disabled) {
        this.addPlugin(Locales, options.Locales);
    }
    if (!options.ReduxDevTools.disabled) {
        this.addPlugin(ReduxDevTools, options.ReduxDevTools);
    }
    if (!options.Router.disabled) {
        this.addPlugin(Router, options.Router);
    }
    if (!options.Screen.disabled) {
        this.addPlugin(Screen, options.Screen);
    }

    this.addEventListener(
        window,
        "load DOMContentLoaded",
        once(function onLoad() {
            _this.page.setHtml5Mode(
                config.html5Mode,
                function onSetHtml5Mode() {
                    var state;

                    _this.emit("load");

                    if (options.restoreAppStateOnLoad) {
                        if ((state = _this.storage.get(_this.appStateKey))) {
                            _this.unsafeSetState(fromJS(JSON.parse(state)));
                        }
                    }

                    _this.page.listen();
                    _this._initted = true;
                }
            );

            _this.addEventListener(
                window,
                "unload",
                once(function onUnload() {
                    _this.storage.set(
                        _this.appStateKey,
                        JSON.stringify(_this.toJSON())
                    );
                })
            );
        })
    );

    page.on("request", function(ctx) {
        _this.emit("request", ctx);
    });
}
State.extend(App);
AppPrototype = App.prototype;

var CREATED_STORE_ERROR_CACHE;
if (process.env.NODE_ENV !== "production") {
    CREATED_STORE_ERROR_CACHE =
        window._CREATED_STORE_ERROR_CACHE ||
        (window._CREATED_STORE_ERROR_CACHE = {});

    AppPrototype.createStore = function(name, initialState) {
        var store = this.store(name),
            message;

        if (store) {
            message =
                "this is propably because of hmr, if not, you are creating a store named `" +
                name +
                "` that is already in state";

            if (!CREATED_STORE_ERROR_CACHE[message]) {
                CREATED_STORE_ERROR_CACHE[message] = true;
                console.error(message);
            }

            this._internal = this._internal.set(
                name,
                fromJS(Object(initialState)).merge(this._internal.get(name))
            );
            return store;
        } else {
            return State.prototype.createStore.call(this, name, initialState);
        }
    };
}

AppPrototype.createForm = createForm;

AppPrototype.initted = function() {
    return this._initted;
};

AppPrototype.plugin = function(PluginClass) {
    return this.pluginByName(PluginClass.pluginName());
};

AppPrototype.pluginByName = function(name) {
    return this._plugins[name];
};

AppPrototype.hasPlugin = function(PluginClass) {
    return !!this.pluginByName(PluginClass.pluginName());
};

AppPrototype.hasPluginByName = function(name) {
    return !!this.pluginByName(name);
};

AppPrototype.addPlugin = function(PluginClass, options) {
    var name = PluginClass.pluginName();

    if (this._plugins[name]) {
        throw new Error("App already has plugin with name " + name);
    }

    this._plugins[name] = new PluginClass(this, options);

    return this;
};

AppPrototype.addEventListener = function(node, type, callback) {
    eventListener.on(node, type, callback);
};

AppPrototype.removeEventListener = function(node, type, callback) {
    eventListener.off(node, type, callback);
};

module.exports = App;

Locales = AppPrototype.Locales = require("./plugins/Locales");
ReduxDevTools = AppPrototype.ReduxDevTools = require("./plugins/ReduxDevTools");
Router = AppPrototype.Router = require("./plugins/Router");
Screen = AppPrototype.Screen = require("./plugins/Screen");
