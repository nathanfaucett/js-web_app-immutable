var immutable = require("immutable"),
    Plugin = require("../Plugin");

var fromJS = immutable.fromJS;

module.exports = ReduxDevTools;

function ReduxDevTools(app) {
    var isJumpingToAction = false,
        _this;

    Plugin.call(this, app);

    if (process.env.NODE_ENV !== "production") {
        if (window.devToolsExtension) {
            _this = this;

            this.devTools = window.devToolsExtension.connect();

            this.devTools.subscribe(function onMessage(message) {
                var state;

                if (
                    message.type === "DISPATCH" &&
                    message.payload.type === "JUMP_TO_ACTION"
                ) {
                    state = fromJS(JSON.parse(message.state));

                    isJumpingToAction = true;
                    app.unsafeSetState(state);
                    app.forceUpdateAll();
                    isJumpingToAction = false;
                }
            });

            this.app.on("update", function onUpdate(name) {
                if (!isJumpingToAction) {
                    _this.devTools.send(
                        {
                            type: "update." + (name || "unknown"),
                            payload: app.store(name).state()
                        },
                        app.toJSON()
                    );
                }
            });
        }
    }

    this.isJumpingToAction = function() {
        return isJumpingToAction;
    };
}
Plugin.extend(ReduxDevTools);

ReduxDevTools.pluginName = function() {
    return "ReduxDevTools";
};
