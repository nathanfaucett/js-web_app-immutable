var layers = require("@nathanfaucett/layers_browser"),
    immutable = require("immutable"),
    ReduxDevTools = require("./ReduxDevTools"),
    Plugin = require("../Plugin");

var Map = immutable.Map,
    fromJS = immutable.fromJS,
    layersRouterPrototype = layers.Router.prototype,
    RouterPrototype;

module.exports = Router;

function Router(app) {
    var _this = this,
        isRouting = false,
        router,
        store;

    Plugin.call(this, app);

    this._data = {};

    router = this._router = new RouterWrapper();
    router._app = app;

    store = this._store = this.app.createStore(Router.storeName(), {
        ctx: {},
        state: null
    });

    store.on("unsafeSetState", function(prevState, nextState) {
        var reduxDevTools = app.plugin(ReduxDevTools),
            prevPathname,
            nextPathname;

        if (
            !reduxDevTools ||
            (reduxDevTools && !reduxDevTools.isJumpingToAction())
        ) {
            store.unsafeSetState(nextState.set("state", null), false);

            if (!isRouting) {
                prevPathname = prevState.get("ctx", Map()).get("pathname");
                nextPathname = nextState.get("ctx", Map()).get("pathname");

                if (prevPathname !== nextPathname) {
                    app.page.go(nextPathname);
                }
            }
        }
    });

    this.app.on("request", function onRequest(ctx) {
        isRouting = true;
        _this.emit("request", ctx);

        router.handler(ctx, function onHandler(error) {
            isRouting = false;

            if (error) {
                _this.emit("requestError", error, ctx);
                throw error;
            } else {
                _this.emit("requestEnd", ctx);
            }
        });
    });

    this.isRouting = function() {
        return isRouting;
    };
}
Plugin.extend(Router);
RouterPrototype = Router.prototype;

Router.pluginName = function() {
    return "Router";
};

Router.storeName = function() {
    return "router";
};

RouterPrototype.setData = function(name, data) {
    this._data[name] = data;
    return this;
};

RouterPrototype.data = function(name) {
    return this._data[name];
};

RouterPrototype.router = function() {
    return this._router;
};

RouterPrototype.store = function() {
    return this._store;
};

RouterPrototype.use = function() {
    return this._router.use.apply(this._router, arguments);
};

RouterPrototype.route = function(state, path, data) {
    return this._router.route(state, path, data);
};

RouterPrototype.scope = function(path) {
    return this._router.scope(path);
};

RouterPrototype.notFound = function(data) {
    var app = this.app;

    if (!data) {
        throw new TypeError("data is required");
    }

    this.setData("not_found", data);

    return function notFound(ctx, next) {
        if (ctx.route) {
            next();
        } else {
            app.setStateFor(Router.storeName(), {
                ctx: getContext(ctx),
                state: "not_found"
            });
            ctx.end();
            next();
        }
    };
};

var RouterWrapperPrototype;

function RouterWrapper(path, parent) {
    layers.Router.call(this, path, parent);
    this._app = null;
    this.Scope = RouterWrapper;
}
layers.Router.extend(RouterWrapper);
RouterWrapperPrototype = RouterWrapper.prototype;

RouterWrapperPrototype.use = function() {
    layersRouterPrototype.use.apply(this, arguments);
    return this;
};

RouterWrapperPrototype.route = function(state, path, data) {
    var _this = this;

    this._app.plugin(Router).setData(state, data);

    layersRouterPrototype.route.call(this, path, function onRoute(ctx, next) {
        _this._app.setStateFor(Router.storeName(), {
            ctx: getContext(ctx),
            state: state
        });
        ctx.end();
        next();
    });

    return this;
};

RouterWrapperPrototype.scope = function(path) {
    var scope = layersRouterPrototype.scope.call(this, path);
    scope._app = this._app;
    return scope;
};

function getContext(ctx) {
    return fromJS({
        fullUrl: ctx.fullUrl,
        params: ctx.params,
        pathname: ctx.pathname,
        query: ctx.query
    });
}
