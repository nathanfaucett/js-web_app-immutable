var EventEmitter = require("@nathanfaucett/event_emitter");

module.exports = Plugin;

function Plugin(app, options) {
	this.app = app;
	this.options = options || {};
}
EventEmitter.extend(Plugin);

Plugin.pluginName = function() {
	throw new Error(
		"pluginName() most be defined and should return unique name to identify Plugin"
	);
};
