module.exports = options => ({
	appUrl: "http://localhost:" + options.PORT,
	apiUrl: "http://localhost:" + options.API_PORT
});
