import { App } from "@nathanfaucett/web_app-immutable";
import config from "./config.json";

class WebAppExampleApp extends App {
	constructor(config) {
		super(config, {
			restoreAppStateOnLoad: false,
			appStateKey: "WebAppExample-State",
			Locales: {
				header: "X-WebAppExample-Locale"
			}
		});

		this.request.defaults.headers.Accept = "application/json";
		this.request.defaults.headers["Content-Type"] = "application/json";
		this.request.defaults.withCredentials = true;
	}
}

export default new WebAppExampleApp(config);
