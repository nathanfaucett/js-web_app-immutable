import React from "react";
import PropTypes from "prop-types";

class Todo extends React.Component {
    constructor(props) {
        super(props);

        this.onClick = () => {
            this.props.onClick(this.props.todo.id);
        };
    }
    render() {
        return (
            <div className="Todo" onClick={this.onClick}>
                {this.props.todo.text}
            </div>
        );
    }
}

Todo.propTypes = {
    todo: PropTypes.object.isRequired,
    onClick: PropTypes.func.isRequired
};

export default Todo;
