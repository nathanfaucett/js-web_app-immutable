import React from "react";
import { Helmet } from "react-helmet";
import PropTypes from "prop-types";
import { injectIntl } from "react-intl";
import Header from "./Header";
import Footer from "./Footer";

class Layout extends React.Component {
	render() {
		let { intl, title } = this.props;

		return (
			<div>
				<Helmet>
					<title>
						{title ? title : intl.formatMessage({ id: "app.name" })}
					</title>
				</Helmet>
				<Header />
				{this.props.children}
				<Footer />
			</div>
		);
	}
}

Layout.propTypes = {
	title: PropTypes.string
};

export default injectIntl(Layout);
