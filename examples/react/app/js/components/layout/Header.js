import React from "react";
import { FormattedMessage } from "react-intl";
import user from "../../stores/user";
import app from "../../app";

class Header extends React.Component {
    constructor(props) {
        super(props);

        this.onSignOut = () => {
            user.signOut(() => {
                app.page.go("/sign_in");
            });
        };
    }
    render() {
        return (
            <div>
                {user.isSignedIn() ? (
                    <button onClick={this.onSignOut}>
                        <FormattedMessage id="sign_in.sign_out" />
                    </button>
                ) : (
                    <a href="/sign_in">
                        <button>
                            <FormattedMessage id="sign_in.sign_in" />
                        </button>
                    </a>
                )}
            </div>
        );
    }
}

export default Header;
