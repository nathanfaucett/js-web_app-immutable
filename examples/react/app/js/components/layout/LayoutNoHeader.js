import React from "react";
import { Helmet } from "react-helmet";
import PropTypes from "prop-types";
import { injectIntl } from "react-intl";
import Footer from "./Footer";

class LayoutNoHeader extends React.Component {
    render() {
        let { intl, title } = this.props;

        return (
            <div>
                <Helmet>
                    <title>
                        {title ? title : intl.formatMessage({ id: "app.name" })}
                    </title>
                </Helmet>
                {this.props.children}
                <Footer />
            </div>
        );
    }
}

LayoutNoHeader.propTypes = {
    title: PropTypes.string
};

export default injectIntl(LayoutNoHeader);
