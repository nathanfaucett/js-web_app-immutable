import React from "react";
import { FormattedMessage } from "react-intl";

class NotFound extends React.Component {
    render() {
        return (
            <div>
                <h1>
                    <FormattedMessage id="error.not_found.code" />
                </h1>
                <p>
                    <FormattedMessage id="error.not_found.message" />
                </p>
            </div>
        );
    }
}

export default NotFound;
