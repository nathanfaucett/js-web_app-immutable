import app from "../app";
import user from "../stores/user";

export default (ctx, next) => {
	user.autoSignIn(error => {
		if (error) {
			app.page.go("/sign_in");
		} else {
			next();
		}
	});
};
