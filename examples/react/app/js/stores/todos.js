import {Map, List} from "immutable";
import uuid from "@nathanfaucett/uuid";
import app from "../app";

const todos = app.createStore("todos", {list: List()});

todos.create = text => {
    todos.updateState(state =>
        state.update("list", list =>
            list.push(Map({id: uuid.v4(), text: text}))
        )
    );
};

todos.remove = id => {
    todos.updateState(state =>
        state.update("list", list =>
            list.remove(list.findIndex(todo => todo.get("id") === id))
        )
    );
};

export default todos;
